<%@page import="hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<style type="text/css">
	  		<%@include file="bootstrap-3.3.7-dist/css/bootstrap.min.css" %>
	</style>
<title>Vlasnik dodaje administratora</title>
</head>

<%
//	int brAdmina = (int) request.getAttribute("brAdmina");
	int brAdmina = Administrator.BROJ_ADMINA;
%>


<html>
<body>
	<jsp:include page="zaglavlje.jsp" />
	<center>
	<p>
		Broj administratora:
		<%=brAdmina%>
	</p>
	<br>
	<form action="<%=request.getContextPath()%>/dodavanjeAdmina"
		method="post">
		<table>
		<tr>
			<td align="left">Username:</td>
			<td><input type="text" id="username" name="username" value='' /></td>
		</tr>
		<tr>
			<td align="left">Lozinka:</td>
			<td><input type="text" id="password" name="password" value='${genkey}' /></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><input type="submit" value="Registriraj" class="btn btn-primary"></td>
		</tr>		
		</table>
	</form>
	</center>
</body>
</html>