<%@page import="hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<style type="text/css">
  		<%@include file="bootstrap-3.3.7-dist/css/bootstrap.min.css" %>
	</style>

	<title>
		Uređivanje podataka
	</title>
</head>

<html>
	<body><br><br><br>
	<% String error = (String) request.getAttribute("error");
		if (error != null) out.println(error);
		String ime = (String) request.getParameter("ime");
		String prezime = (String) request.getParameter("prezime");
		String kontakt = (String) request.getParameter("kontakt");
		String username = (String) request.getParameter("username");
		String lozinka = (String) request.getParameter("lozinka");
		
		Administrator admin = (Administrator) request.getSession().getAttribute("administrator");
		if(ime == null && admin != null) {
			ime = admin.getIme();
			prezime = admin.getPrezime();
			kontakt = admin.getKontakt();
			username = admin.getUsername();
			lozinka = admin.getLozinka();
		}
		if(ime == null) ime = "";
		if(prezime == null) prezime = "";
		if(kontakt == null) kontakt = "";
		if(username == null) username = "";
		if(lozinka == null) lozinka = "";
	%>
		<center><h1>Uredi podatke</h1></center>
		<br>
		<form action="<%= request.getContextPath() %>/urediPodatke" method="post">
			<table align="center">
				<tr>
					<th align="right">Ime:</th>
					<td><input type="text" id="ime" name="ime" value='<%=ime%>' /></td>
				</tr>
				<tr>
					<th align="right">Prezime:</th>
					<td><input type="text" id="prezime" name="prezime" value='<%=prezime%>' /></td>
				</tr>
				<tr>
					<th align="right">Kontakt:</th>
					<td><input type="text" id="kontakt" name="kontakt" value='<%=kontakt%>' /></td>
				</tr>
				<tr>
					<th align="right">Username:</th>
					<td><input type="text" id="username" name="username" value='<%=username%>' /></td>
				</tr>
				<tr>
					<th align="right">Lozinka:</th>
					<td><input type="password" id="lozinka" name="lozinka" value='<%=lozinka%>' /></td>
				</tr>
				<tr>
					<td colspan="2" align="right"><input type="submit" value="Prijavi se" class="btn btn-primary"></td>
				</tr>
			</table>
		</form>
	</body>
</html>