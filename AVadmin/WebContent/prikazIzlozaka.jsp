<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<style type="text/css">
	  		<%@include file="bootstrap-3.3.7-dist/css/bootstrap.min.css" %>
	</style>
	<title>Izlošci</title>
</head>
<body>
	<jsp:include page="zaglavlje.jsp" />
	<center><h1>Objekt: ${naziv}</h1></center>
	<h1>
	<%  String s = (String) request.getAttribute("greska");
		if(s != null) {
			out.println(s);
		}
	%>
	</h1>
	<div class="container">
	<ul class="list-group">
		<li class="list-group-item">
			<table class="table">
			<thead>
				<tr>
					<th>Naziv</th>
					<th>Audio zapis</th>
					<th>Opis</th>
				</tr>
			</thead>
		<c:forEach var="izlozak" items="${izlosci}">
		
			
			<tr>
				<td><a href="<%= request.getContextPath() %>/admin/objekti/${sifra}/${izlozak.sifIzlozak}">
					${izlozak.naziv}
				</a></td>
				<td><audio controls>
					<source src="<%= request.getContextPath()%>/dohvatiIzlozak?qrkod=${izlozak.sifIzlozak}&audio=325" 
					type="audio/mpeg">
				</audio></td>
			<td><a href="<%= request.getContextPath()%>/dohvatiIzlozak?qrkod=${izlozak.sifIzlozak}">${izlozak.opis}</a></td>
			</tr>
		</c:forEach>
		</table>
		</li>
	</ul>	
	</div>	
	
	<center>
	<form action="<%= request.getContextPath() %>/admin/objekti/${sifra}" method="post" enctype="multipart/form-data" >
		<table>
			<tr>
					<th align="left">Naziv:</th>
					<td><input type="text" id="naziv" name="naziv" placeholder='Naziv' /></td>
			</tr>
			<tr>
					<th align="left">Audio:</th>
					<td><input type="file" name="audio" id="audio" /></td>
			</tr>
			<tr>
            		<th align="left">Opis:</th>           
            </tr>
			<tr>
            		<td colspan="2" align="left"><textarea cols="125" rows="30" id="opis" name="opis" > </textarea> </td>       
            </tr>
			<tr>
            		<td colspan="2" align="right"><input type="submit" value="Novi izlozak" class="btn btn-primary" /> </td>         
            </tr>
		</table>
	</form>
	</center>
		
	
</body>
</html>