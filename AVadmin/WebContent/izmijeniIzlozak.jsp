<%@page import="java.nio.file.Files"%>
<%@page import="hr.fer.zemris.opp.projekt.audio_vodic.model.Izlozak"%>
<%@page import="java.util.List"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.Path"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<style type="text/css">
	  		<%@include file="bootstrap-3.3.7-dist/css/bootstrap.min.css" %>
			body {
				margin-left: 80px;
			}
			head{
				margin-left: 80px;
			}
	</style>
	<title>Uređivanje izloška</title>
</head>
<body>
	<jsp:include page="zaglavlje.jsp" />
	<center><h1>Objekt: ${naziv}</h1></center>
	<p>
	<%  String s = (String) request.getAttribute("greska");
		if(s != null) {
			out.println(s);
		}
		
		Izlozak izlozak = (Izlozak) request.getAttribute("izlozak");
		Path opis = Paths.get(izlozak.getOpis().trim());
		List<String> lines = Files.readAllLines(opis);
		StringBuilder sb = new StringBuilder();
		for(String line : lines) {
			sb.append(line);
			sb.append("\n");
		}
		String opisText = sb.toString();
	%>
	</p>
	<center>
	<form action="<%= request.getContextPath() %>/admin/objekti/${sifra}/${izlozak.sifIzlozak}"
	 method="post" enctype="multipart/form-data" >
		<table align="left">
			<tr>
					<th align="left">Naziv:</th>
					<td><input type="text" id="naziv" name="naziv" value='${izlozak.naziv}' /></td>
			</tr>
			<tr>
					<th align="left">Trenutno pohranjeni audio zapis:</th>
					<td>
						<audio controls>
				 			<source src="<%= request.getContextPath()%>/dohvatiIzlozak?qrkod=${izlozak.sifIzlozak}&audio=325" 
				 			type="audio/mpeg">
						</audio>
					</td>
			</tr>
			<tr>
					<th align="left">Dodaj novi audio zapis:</th>
					<td><input type="file" name="audio" id="audio" /></td>
			</tr>	
			<tr>
            		<th align="left">Opis:</th>           
            </tr>
		</table>
		<br><br><br><br><br>
		<div class="form-group">
			<textarea class="form-control" cols="125" rows="30" id="opis" name="opis"> <%= opisText %> </textarea>
		</div>
		<input type="submit" value="Promijeni izložak" class="btn btn-primary" /> <br><br>
	</form>
	
	<a href="<%= request.getContextPath() %>/admin/objekti/${sifra}/${izlozak.sifIzlozak}/del">Izbriši izložak</a>
	</center>
</body>
</html>