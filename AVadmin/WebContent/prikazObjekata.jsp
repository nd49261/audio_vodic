<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Objekti</title>
</head>
<body>
	<jsp:include page="zaglavlje.jsp" />
	<div class="list-group">
		<c:forEach var="objekt" items="${objekti}">
			<a class="list-group-item" href="<%= request.getContextPath()  %>/admin/objekti/${objekt.sifObjekt}">${objekt}</a>
		</c:forEach>
	</div>

	<form action="<%= request.getContextPath() %>/admin/objekti" method="post">
		<br> <center><input type="text" id="naziv" name="naziv" placeholder="Naziv" />
		<input type="submit" value="Novi objekt" class="btn btn-primary" /></center> <br>
	</form>
		
</body>
</html>