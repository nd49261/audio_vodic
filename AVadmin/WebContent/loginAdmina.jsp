<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<style type="text/css">
  		<%@include file="bootstrap-3.3.7-dist/css/bootstrap.min.css" %>
	</style>
	
	<title>
		Login
	</title>
</head>

<html>
	<body><br><br><br>
	<% String error = (String)request.getAttribute("error");
		if (error != null) out.println(error);
		String username = request.getParameter("username") == null ? "" : (String)request.getParameter("username");
		String lozinka = username == "" ? "" : (String)request.getParameter("lozinka");
	%>
		<center><h1>Login</h1></center>
		<br>
		<form action="<%= request.getContextPath() %>/login" method="post">
			<table align="center">
				<tr>
					<th align="right">Username:</th>
					<td><input type="text" id="username" name="username" placeholder="Username" value='<%=username%>'></td>
				</tr>
				<tr>
					<th align="right">Lozinka:</th>
					<td><input type="password" id="lozinka" name="lozinka" placeholder="Lozinka" value='<%=lozinka%>'></td>
				</tr>
				<tr>
					<td colspan="2" align="right"><input type="submit" value="Prijavi se" class="btn btn-primary"></td>
				</tr>
			</table>
		</form>
	</body>
</html>