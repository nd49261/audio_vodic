<%@page import="hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Statistika</title>
</head>
<body>
	<jsp:include page="zaglavlje.jsp" />
	<%
		Administrator admin = (Administrator) request.getSession().getAttribute("administrator");	
	%>
	
	<div class="container">
		<ul class="list-group">
			<li class="list-group-item">
				<table class="table">
				<thead>
					<tr>
						<th>Naziv izloška</th>
						<th>Broj registriranih korisnika koji izložak preuzeli</th>
						<th>Broj reprodukcija audio zapisa</th>
						<th>Ukupan broj preuzimanja izloška</th>
					</tr>
				</thead>
			<% 
			
				List<Integer> podatci =  (List<Integer>) request.getAttribute("podatci");
				
				for(int i = 0, len = podatci.size();i < len;i += 4) {
			%> 
				<tr>
					<td>
						<%= admin.dohvatiIzlozak(podatci.get(i)).getNaziv() %>
					</td>
					<td>
						<%= podatci.get(i + 1) %>
					</td>
					<td>
						<%= podatci.get(i + 2) %>
					</td>
					<td>
						<%= podatci.get(i + 3) %>
					</td>	
			
				</tr>
			<% 
					
				}
			%>
			</table>
			</li>
		</ul>	
	</div>	
	
		
</body>
</html>