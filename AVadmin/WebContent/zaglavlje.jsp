<%@page import="hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<head>
  
	<style type="text/css">
  		<%@include file="bootstrap-3.3.7-dist/css/bootstrap.min.css" %>
	</style>

</head>
<body>
<%
	Administrator admin = (Administrator) session.getAttribute("administrator");
	boolean loggedIn = admin != null;
	//boolean jeVlasnik = session.getAttribute("vlasnik") != null;
	String name =  admin.getIme() + " " + admin.getPrezime();

	if(loggedIn) {
%>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<%= request.getContextPath() %>/objekti">Prijavljeni ste kao: <%= name %></a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <c:if test="${vlasnik!=null}"><li><a href="<%= request.getContextPath() %>/dodavanjeAdmina">Dodaj administratora</a></li></c:if>
      <li><a href="<%= request.getContextPath() %>/urediPodatke">Uredi osobne podatke</a></li>
      <li><a href="<%= request.getContextPath() %>/odjava">Odjavi se</a></li>
      <li><a href="<%= request.getContextPath() %>/statistika">Pregledaj statistiku</a></li>
    </ul>
  </div>
</nav>

<% } else { %>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Niste prijavljeni</a>
		</div>
	</div>
</nav>
<% } %>
</body>