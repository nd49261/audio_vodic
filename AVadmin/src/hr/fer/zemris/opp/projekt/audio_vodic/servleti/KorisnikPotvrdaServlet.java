package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.baza.Inicijalizacija;

@WebServlet("/potvrda/*")
public class KorisnikPotvrdaServlet extends HttpServlet {
	private static final long serialVersionUID = 325L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String[] parameters = req.getPathInfo().substring(1).split("[/]");

		String kljuc = parameters[0];
		if (!kljuc.contains("420")) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}

		ServletOutputStream stream = resp.getOutputStream();
		Connection veza = null;
		try {
			veza = DriverManager.getConnection(Inicijalizacija.url);
			PreparedStatement pst = veza.prepareStatement("select * from Korisnik where potvrda = ?");
			pst.setString(1, kljuc);
			ResultSet res = pst.executeQuery();

			if (!res.isBeforeFirst()) {
				stream.println("Neispravan ključ.");
				pst.close();
				veza.close();
				return;
			}
			
			pst.close();
			
			pst = veza.prepareStatement("UPDATE Korisnik set potvrda = 'DA' where potvrda = ?");
			pst.setString(1, kljuc);
			pst.executeUpdate();
			pst.close();
			
			stream.println("Čestitamo! Uspješno ste potvrdili svoju prijavu. Uživajte u nažim sadržajima.");

		} catch (Exception e) {
			stream.println("Greška pri povezivanju s bazom!");
			return;
		}
	}
}
