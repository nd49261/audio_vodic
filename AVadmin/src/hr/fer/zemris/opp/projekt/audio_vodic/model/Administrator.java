package hr.fer.zemris.opp.projekt.audio_vodic.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.opp.projekt.audio_vodic.baza.BazaSuceljeAdmin;
import hr.fer.zemris.opp.projekt.audio_vodic.baza.Inicijalizacija;
import hr.fer.zemris.opp.projekt.audio_vodic.crypt.Util;

public class Administrator implements BazaSuceljeAdmin {
	
	public static int BROJ_ADMINA = -1; 
	private int sifAdmin;
	private String ime;
	private String prezime;
	private String kontakt;
	private String username;
	private String lozinka;
	protected Connection veza;
	
	public Administrator(int sifAdmin, String ime, String prezime, String kontakt, String username, String lozinka) {
		this.sifAdmin = sifAdmin;
		this.ime = ime;
		this.prezime = prezime;
		this.kontakt = kontakt;
		this.username = username;
		this.lozinka = lozinka;
	}
	
	public Administrator(String username, String lozinka) {
		this.username = username;
		this.lozinka = lozinka;
	}

	public int getSifAdmin() {
		return sifAdmin;
	}


	public void setSifAdmin(int sifAdmin) {
		this.sifAdmin = sifAdmin;
	}


	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public String getPrezime() {
		return prezime;
	}


	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}


	public String getKontakt() {
		return kontakt;
	}


	public void setKontakt(String kontakt) {
		this.kontakt = kontakt;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getLozinka() {
		return lozinka;
	}


	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public boolean prijava() {
		boolean ima = false;
		try {
			uspostaviVezu();
			PreparedStatement pstat = veza.prepareStatement("SELECT ad.sifAdmin, ad.kontakt, ad.ime, ad.prezime from Administrator as ad where username =? and lozinka =?");
			pstat.setString(1, username);
			pstat.setString(2, Util.convertIntoSHA(lozinka));
			ResultSet res = pstat.executeQuery();
			
//			Statement st = veza.createStatement();
//			ResultSet res = st.executeQuery("select ad.sifAdmin, ad.kontakt, ad.ime, ad.prezime from Administrator as ad where username = '" + username + "' and lozinka ='"
//			+ Util.convertIntoSHA(lozinka) + "'");
			
			ima = res.next();
			if(ima) {
				this.sifAdmin = res.getInt(1);
				this.kontakt = res.getString(2);
				this.ime = res.getString(3);
				this.prezime = res.getString(4);
			}
			pstat.close();
			veza.close();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return ima;
	}
	
	public void odjava() {
		//TODO Maknuti ovo ako se ne koristi.
	}
	
	public boolean spremiSvojePodatke() {
		try {
			uspostaviVezu();
			Statement st = veza.createStatement();
			ResultSet rs = st.executeQuery("SELECT * from Administrator WHERE username = '" + username + "' and sifAdmin <> " + sifAdmin);
			if(rs.next()) {
				st.close();
				return false;
			}
			st.close();
			
			st = veza.createStatement();
			st.executeUpdate("UPDATE Administrator "
					+ "SET ime = '" + ime + "', prezime ='" + prezime + "', kontakt ='" + kontakt
					+ "', lozinka = '" + Util.convertIntoSHA(lozinka) + "', username = '" + username + "'"
					+ " WHERE sifAdmin = " + sifAdmin);
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally{
			try {
				if(veza != null && !veza.isClosed()) veza.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
			
		}
		
		return true;
	}
	@Override
	public Izlozak dohvatiIzlozak(int sifIzlozak) throws SQLException {
		uspostaviVezu();
		Izlozak izlozak = null;
		
		try {
			Statement statement = veza.createStatement();
			ResultSet resultSet = statement.executeQuery(
					"select Izlozak.naziv, AudioZapis.relAdr, Opis.relAdr "
					+ "from Izlozak join ima on Izlozak.sifIzlozak = ima.sifIzlozak "
					+ "join jeOpisan on Izlozak.sifIzlozak = jeOpisan.sifIzlozak "
					+ "join AudioZapis on AudioZapis.sifAudio = ima.sifAudio "
					+ "join Opis on Opis.sifOpis = jeOpisan.sifOpis "
					+ "where Izlozak.sifIzlozak = " + sifIzlozak);

			if(!resultSet.next()) {
				statement.close();
				return null;
			}

			izlozak = new Izlozak(sifIzlozak, resultSet.getString(1), resultSet.getString(2), resultSet.getString(3));
			
			statement.close();
			
		} catch (SQLException sqle) {
			System.err.println("Neuspjeli dohvat izloška: " + sifIzlozak);
			sqle.printStackTrace();
		} finally {
			if(veza != null && !veza.isClosed())veza.close();
		}
		
		return izlozak;
	}

	@Override
	public boolean pohraniIzlozak(Izlozak izlozak, int sifObjekt) throws SQLException {
		uspostaviVezu();
		
		try {
			Statement statement = veza.createStatement();
			
			statement.execute("BEGIN TRAN");
			statement.close();
			statement = veza.createStatement();
			
			ResultSet resultSet = statement.executeQuery(
					"SELECT sifIzlozak, naziv from Izlozak WHERE sifIzlozak = " + izlozak.getSifIzlozak());
			boolean postoji = resultSet.next();
			statement.close();
			PreparedStatement pst;
			
			if(postoji) {
				pst = veza.prepareStatement(
						"UPDATE Izlozak SET naziv = ? WHERE sifIzlozak = ?");
				pst.setString(1, izlozak.getNaziv());
				pst.setInt(2, izlozak.getSifIzlozak());
				pst.executeUpdate();
				pst.close();
			} else {
				pst = veza.prepareStatement("INSERT INTO Izlozak(naziv) VALUES (?)"
						, Statement.RETURN_GENERATED_KEYS);
				pst.setString(1, izlozak.getNaziv());
				pst.executeUpdate();
				ResultSet r = pst.getGeneratedKeys();
				r.next();
				int sifIzlozak = r.getInt(1); 
				pst.close();
				
				String pathAudio = "res/izlosci/izl" + sifIzlozak + "/audio.mp3";
				String pathOpis = "res/izlosci/izl" + sifIzlozak + "/opis.txt";
				
				pst = veza.prepareStatement("INSERT INTO Opis(relAdr) VALUES (?)"
						, Statement.RETURN_GENERATED_KEYS);
				pst.setString(1, pathOpis);
				pst.executeUpdate();
				r = pst.getGeneratedKeys();
				r.next();
				int sifOpis = r.getInt(1);
				pst.close();
				
				pst = veza.prepareStatement("INSERT INTO AudioZapis(relAdr, brojReprodukcija) VALUES (?, ?)"
						, Statement.RETURN_GENERATED_KEYS);
				pst.setString(1, pathAudio);
				pst.setInt(2, 0);
				pst.executeUpdate();
				r = pst.getGeneratedKeys();
				r.next();
				int sifAudio = r.getInt(1);
				pst.close();
				
				pst = veza.prepareStatement("INSERT INTO jeOpisan(sifOpis, sifIzlozak) VALUES (?, ?)");
				pst.setInt(1, sifOpis);
				pst.setInt(2,  sifIzlozak);
				pst.executeUpdate();
				pst.close();
								
				pst = veza.prepareStatement("INSERT INTO ima(sifAudio, sifIzlozak) VALUES (?, ?)");
				pst.setInt(1, sifAudio);
				pst.setInt(2, sifIzlozak);
				pst.executeUpdate();
				pst.close();
				
				izlozak.setSifIzlozak(sifIzlozak);
				izlozak.setAudioZapis(pathAudio);
				izlozak.setOpis(pathOpis);
				
				System.err.println(this.sifAdmin);
				pst = veza.prepareStatement("INSERT INTO jeDodao(sifAdmin, sifIzlozak) VALUES (?, ?)");
				pst.setInt(1, this.sifAdmin);
				pst.setInt(2, sifIzlozak);
				pst.executeUpdate();
				pst.close();
				
				pst = veza.prepareStatement("INSERT INTO seNalazi(sifObjekt, sifIzlozak) VALUES (?, ?)");
				pst.setInt(1, sifObjekt);
				pst.setInt(2, sifIzlozak);
				pst.executeUpdate();
				pst.close();
				
				
			}
			
			statement = veza.createStatement();
			statement.execute("COMMIT");
			statement.close();
			
		} catch (SQLException sqle) {
			System.err.println("Neuspjelo spremanje izloska: " + izlozak.getSifIzlozak());
			sqle.printStackTrace();
			Statement st = veza.createStatement();
			st.execute("ROLLBACK");
			return false;
		} finally {
			if(veza != null) veza.close();
		}
		return true;
	}

	@Override
	public Objekt dohvatiObjekt(int sifObjekt) throws SQLException {
		uspostaviVezu();
		Objekt objekt = null;
		try {
			Statement statement = veza.createStatement();
			
			ResultSet resultSet = statement.executeQuery(
					"SELECT naziv from Objekt where sifObjekt =" + sifObjekt );
			
			if(!resultSet.next()) {
				statement.close();
				return null;
			}
			objekt = new Objekt(sifObjekt, resultSet.getString(1));
			statement.close();
			
			statement = veza.createStatement();
			resultSet = statement.executeQuery("SELECT sifIzlozak FROM seNalazi WHERE sifObjekt = " + sifObjekt);
			
			List<Integer> sifIzlozaka = new ArrayList<>();
			while(resultSet.next()) sifIzlozaka.add(resultSet.getInt(1));
			
			resultSet.close();
			statement.close();
			
			for(int i : sifIzlozaka) {
				objekt.dodajIzlozak(dohvatiIzlozak(i));
			}
		} catch (SQLException sqle) {
			System.err.println("Neuspješan dohvat objekta: " + sifObjekt);
			return null;
		} finally {
			if(veza != null && !veza.isClosed())veza.close();
		}
		return objekt;
	}
	
	@Override
	public List<Objekt> dohvatiSveObjekte() throws SQLException {
		uspostaviVezu();
		List<Objekt> ret = new ArrayList<>();
		
		try {
			Statement statement = veza.createStatement();
			ResultSet resultSet = statement.executeQuery(
					"SELECT sifObjekt, naziv from Objekt");
			
			while(resultSet.next()) {
				ret.add(new Objekt(resultSet.getInt(1), resultSet.getString(2)));
			}
			
			statement.close();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			System.err.println("Neuspjelo dohvaćanje objekata");
		} finally {
			veza.close();
		}
		
		return ret;
	}

	@Override
	public boolean pohraniObjekt(Objekt objekt) throws SQLException {
		uspostaviVezu();
		try {
			Statement statement = veza.createStatement();
			
			ResultSet resultSet = statement.executeQuery(
					"SELECT sifObjekt, naziv from Objekt WHERE sifObjekt = " + objekt.getSifObjekt());
			boolean postoji = resultSet.next();
			statement.close();
			PreparedStatement pst;
			
			if(postoji) {
				pst = veza.prepareStatement(
						"UPDATE Objekt SET naziv = ? WHERE sifObjekt = ?");
				pst.setString(1, objekt.getNaziv());
				pst.setInt(2, objekt.getSifObjekt());
			} else {
				pst = veza.prepareStatement("INSERT INTO Objekt(naziv) VALUES (?)");
				pst.setString(1, objekt.getNaziv());
			}
			
			pst.executeUpdate();
			pst.close();
		} catch (SQLException sqle) {
			System.err.println("Neuspjelo spremanje objekta: " + objekt.getSifObjekt());
			return false;
		} finally {
			veza.close();
		}
		
		return true;
	}

	@Override
	public void uspostaviVezu() {
		try {
			if(veza != null && !veza.isClosed()) return;
			veza = DriverManager.getConnection(Inicijalizacija.url);
		} catch (SQLException e) {
			System.err.println("Nemoguće dobiti vezu.");
		}
	}

	@Override
	public void izbrisiIzlozak(int sifIzlozak) throws SQLException {
		uspostaviVezu();
		Statement statement = veza.createStatement();
		
		statement.executeUpdate(
				"DELETE FROM Izlozak WHERE sifIzlozak = " + sifIzlozak);
		statement.close();
		
		veza.close();
	}

}
