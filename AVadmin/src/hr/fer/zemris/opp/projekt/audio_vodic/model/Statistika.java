package hr.fer.zemris.opp.projekt.audio_vodic.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import hr.fer.zemris.opp.projekt.audio_vodic.baza.Inicijalizacija;

public class Statistika {
	
	public static void korisnikReproduciraAudio(int sifAudio) {
		try {
			Connection veza = DriverManager.getConnection(Inicijalizacija.url);
			Statement st = veza.createStatement();
			st.executeUpdate(
					"UPDATE AudioZapis SET brojReprodukcija = brojReprodukcija + 1 "
				  + "WHERE sifAudio = " + sifAudio);

			st.close();
			veza.close();
		} catch (SQLException e) {
			System.err.println("Korisnik reproducira audio greska");
		}
		
	}

	
	public static void preuzetiPodatciOIzlosku(int sifKorisnik, int sifIzlozak) {
		try {
			Connection veza = DriverManager.getConnection(Inicijalizacija.url);
			Statement st = veza.createStatement();
			int rows = st.executeUpdate(
					"UPDATE jePreuzeo SET kolikoPuta = kolikoPuta + 1"
				  + "WHERE sifKorisnik = " + sifKorisnik + "and sifIzlozak = " + sifIzlozak);
			st.close();
			if(rows < 1) {
				st = veza.createStatement();
				st.executeUpdate("INSERT INTO jePreuzeo (sifKorisnik, sifIzlozak, kolikoPuta) "
						       + "VALUES (" + sifKorisnik + ", " + sifIzlozak + ", " + 1 + ")");
			}
			
			st.close();
			veza.close();
		} catch (SQLException e) {
			System.err.println("Preuzeti podatci o izlošku greska");
		}
	}

	
	public static Map<Integer, Integer> brojAudioReprodukcija() {
		Map<Integer, Integer> ret = new HashMap<>();
		try {
			Connection veza = DriverManager.getConnection(Inicijalizacija.url);
			Statement st = veza.createStatement();
			ResultSet rs = st.executeQuery(
					"SELECT sifAudio, brojReprodukcija FROM AudioZapis");
			while(rs.next()) {
				ret.put(rs.getInt(1), rs.getInt(2));
			}
			st.close();
			veza.close();
		} catch (SQLException e) {
			System.err.println("Broj audio reprodukcija greska");
		}
		
		
		return ret;
	}
	
	public static Map<Integer, Integer> brojKorisnikaKojiSuReproduciraliAudio() {
		Map<Integer, Integer> ret = new HashMap<>();
		try {
			Connection veza = DriverManager.getConnection(Inicijalizacija.url);
			Statement st = veza.createStatement();
			ResultSet rs = st.executeQuery(
					"SELECT Iz.sifIzlozak, COUNT(sifKorisnik) " +
					"FROM Izlozak as Iz LEFT OUTER JOIN jePreuzeo ON Iz.sifIzlozak = jePreuzeo.sifIzlozak " +
					"GROUP BY Iz.sifIzlozak");
			while(rs.next()) {
				ret.put(rs.getInt(1), rs.getInt(2));
			}
			st.close();
			veza.close();
		} catch (SQLException e) {
			System.err.println("Broj korisnika koji su reproducirali audio greska");
		}
		return ret;
	}
	
	public static Map<Integer, Integer> brojPreuzimanjaIzlozaka() {
		Map<Integer, Integer> ret = new HashMap<>();
		try {
			Connection veza = DriverManager.getConnection(Inicijalizacija.url);
			Statement st = veza.createStatement();
			ResultSet rs = st.executeQuery(
					"SELECT sifIzlozak, SUM(kolikoPuta) FROM jePreuzeo "
				  + "GROUP BY sifIzlozak");
			while(rs.next()) {
				ret.put(rs.getInt(1), rs.getInt(2));
			}
			st.close();
			veza.close();
		} catch (SQLException e) {
			System.err.println("Broj preuzimanja izlozaka greska");
		}
		
		return ret;
	}
}
