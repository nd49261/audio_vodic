package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.crypt.Util;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Vlasnik;

@WebServlet("/dodavanjeAdmina")
public class DodavanjeAdminaServlet extends HttpServlet {

	private static final long serialVersionUID = 325L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Vlasnik vlasnik = (Vlasnik) req.getSession().getAttribute("vlasnik");
		
		if(vlasnik == null) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		String randomKey = generateKey();
		req.setAttribute("genkey", randomKey);
	
		req.getRequestDispatcher("/dodavanjeAdmina.jsp").forward(req, resp);
	}
	
	private String generateKey() {
		Random rand = new Random();
		byte[] bytes = new byte[8]; 
		rand.nextBytes(bytes);
		String generated = Util.bytetohex(bytes);
		int gdje = rand.nextInt(generated.length() - 4);
		
		String prvi = generated.substring(0, gdje).toUpperCase();
		String drugi = generated.substring(gdje).toUpperCase();
		return prvi + "325" + drugi;
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Vlasnik vlasnik = (Vlasnik) req.getSession().getAttribute("vlasnik");
		
		if(vlasnik == null) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		String username = req.getParameter("username");
		String lozinka = req.getParameter("password");
		String randomKey = generateKey();
		req.setAttribute("genkey", randomKey);
		
		if(vlasnik.dodajAdmina(username, lozinka)) {
			req.getRequestDispatcher("/admin/objekti").forward(req, resp);
		} else {
			req.getRequestDispatcher("/dodavanjeAdmina.jsp").forward(req, resp);
		}
	}
}
