package hr.fer.zemris.opp.projekt.audio_vodic.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import hr.fer.zemris.opp.projekt.audio_vodic.crypt.Util;
import hr.fer.zemris.opp.projekt.audio_vodic.form.KorisnikRegForm;

public class NeregistriraniKorisnik extends Korisnik {
	private String kljuc;
	
	public NeregistriraniKorisnik(){
		super();
	}
	
	public NeregistriraniKorisnik(KorisnikRegForm form) {
		if(!form.getIspravan()) return;
		this.ime = form.getIme();
		this.prezime = form.getPrezime();
		this.username = form.getUsername();
		this.lozinka = form.getLozinka();
		this.email = form.getEmail();
	}
	
	public String getKljuc() {
		return kljuc;
	}
	
	/*public String registrirajSe() {
		uspostaviVezu();
		StringBuilder sb = new StringBuilder();
		try {
			Statement st = veza.createStatement();
			ResultSet res = st.executeQuery("select * from Korisnik where username = '" + username + "'");
			if(res.next()) {
				sb.append("Username se već koristi\n");
				st.close();
				veza.close();
				return sb.toString();
			}
			st.close();
			
			st = veza.createStatement();
			st.executeUpdate("insert into Korisnik(ime, prezime, email, username, lozinka) values ('" +
					ime + "', '" + prezime + "', '" + email + "', '" + username + "', '" + 
					Util.convertIntoSHA(lozinka)+ "')");
			
			st.close();
			veza.close();
		} catch(Exception e) {
			
			sb.append("Greška pri povezivanju s bazom!\n");
			
			return sb.toString();
		} finally {
			try {
				if(veza != null && !veza.isClosed()) veza.close();
			} catch (SQLException ignorable) {}
		}
		
		sb.append("OK\n");
		return sb.toString();
	}*/
	
	public String registrirajSe() {
		uspostaviVezu();
		StringBuilder sb = new StringBuilder();
		try {
			PreparedStatement pst = veza.prepareStatement("select * from Korisnik where username = ?");
			pst.setString(1, username);
			ResultSet res = pst.executeQuery();
			
			if (res.isBeforeFirst()) {
				sb.append("Username se već koristi");
				pst.close();
				veza.close();
				return sb.toString();
			}
			pst.close();
	
			kljuc = generateKey();
			pst = veza.prepareStatement(
					"insert into Korisnik (ime, prezime, email, username, lozinka, potvrda) values (?, ?, ?, ?, ?, ?)");
			pst.setString(1, ime);
			pst.setString(2, prezime);
			pst.setString(3, email);
			pst.setString(4, username);
			pst.setString(5, Util.convertIntoSHA(lozinka));
			pst.setString(6, kljuc);
			pst.executeUpdate();
	
			pst.close();
			veza.close();
		} catch(Exception e) {
			
			sb.append("Greška pri povezivanju s bazom!");
			return sb.toString();
		} finally {
			try {
				if(veza != null && !veza.isClosed()) veza.close();
			} catch (SQLException ignorable) {}
		}
		sb.append("OK");
		return sb.toString();
	}
	
	private String generateKey() {
		Random rand = new Random();
		byte[] bytes = new byte[8];
		rand.nextBytes(bytes);
		String generated = Util.bytetohex(bytes);
		int gdje = rand.nextInt(generated.length() - 4);

		String prvi = generated.substring(0, gdje).toUpperCase();
		String drugi = generated.substring(gdje).toUpperCase();
		return prvi + "420" + drugi;
	}
}
