package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.baza.Inicijalizacija;

/**
 * Servlet koji pokazuje glavne informacije - trenutno probni servlet
 * 
 * @author Martin Šeler
 *
 */
@WebServlet("/sasvimnestotrece")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 325L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Connection connection = null;

		List<String> izlazi = new ArrayList<>();
		
		for (String ime : Inicijalizacija.imenaTablica) {
			izlazi.add("========================== " + ime + " =============================");
		}
		
		try {
			connection = DriverManager.getConnection(Inicijalizacija.url);
			for (String ime : Inicijalizacija.imenaTablica) {
				
				try {
					
					Statement statement = connection.createStatement();
					ResultSet resultSet = statement.executeQuery("SELECT * FROM  " + ime);
					//ResultSet resultSet = statement.executeQuery("SELECT * FROM  Administrator");
					izlazi.add("========================== " + ime + " =============================");
					
					while (resultSet.next()) {
						izlazi.add(resultSet.getString(1) + ", " + resultSet.getString(2));
					}

				} catch (Exception notIgnorable) {
					notIgnorable.printStackTrace();
				}
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		req.setAttribute("izlazi", izlazi);
		req.getRequestDispatcher("/sasvimnestotrece.jsp").forward(req, resp);
	}
}
