package hr.fer.zemris.opp.projekt.audio_vodic.baza;

import java.sql.SQLException;
import java.util.List;

import hr.fer.zemris.opp.projekt.audio_vodic.model.Izlozak;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Objekt;

/**
 * @author Matija Kušec
 * 
 * Sučelje kojim administrator komunicira s bazom podataka.
 *
 */
public interface BazaSuceljeAdmin {
	
	/**
	 * Metoda koja uspostavlja vezu s bazom podataka.
	 * 
	 * @throws SQLException
	 */
	public void uspostaviVezu() throws SQLException;

	/**
	 * Metoda koja vraća izločak za priloženu šifru.
	 * 
	 * @param sifIzlozak - sifra traženog izloška
	 * @return traženi izložak
	 * @throws SQLException
	 */
	public Izlozak dohvatiIzlozak(int sifIzlozak) throws SQLException;

	/**
	 * Metoda koja pohranjuje dani izložak.
	 * 
	 * @param izlozak - izložak koji treba pohraniti
	 * @return true ako je izložak uspješno pohranjen, inače false
	 * @throws SQLException
	 */
	public boolean pohraniIzlozak(Izlozak izlozak, int sifObjekt) throws SQLException;
	
	/**
	 * Metoda koja vraća objekt muzeja za danu šifru objekta.
	 * 
	 * @param sifObjekt - šifra traženog objekta
	 * @return traženi objekt
	 * @throws SQLException
	 */
	public Objekt dohvatiObjekt(int sifObjekt) throws SQLException;
	
	/**
	 * Metoda koja pohranjuje dani objekt.
	 * 
	 * @param objekt - objekt koji treba pohraniti
	 * @return true ako je objekt uspješno pohranjen, inače false
	 * @throws SQLException
	 */
	public boolean pohraniObjekt(Objekt objekt) throws SQLException;
	
	/**
	 * Metoda koja vraća listu 'objekata'.
	 * 
	 * @return lista objekata
	 * @throws SQLException
	 */
	public List<Objekt> dohvatiSveObjekte() throws SQLException;
	
	public void izbrisiIzlozak(int sifIzlozak) throws SQLException;
}
