package hr.fer.zemris.opp.projekt.audio_vodic.baza;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import hr.fer.zemris.opp.projekt.audio_vodic.crypt.Util;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator;

import java.util.List;
import java.util.Properties;
import java.util.ArrayList;

@WebListener
public class Inicijalizacija implements ServletContextListener {
	public static String url; 
//	= "jdbc:sqlserver://audiovodicserver.database.windows.net:1433;database=AVbaza;"
//			+ "user=Miljenko@audiovodicserver;password=plavo-smeđi Lucky Strike bez aditiva;encrypt=true;"
//			+ "trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";

	public static String DB_CON_STRING = "/WEB-INF/db.properties";
	
	/*
	public static String url = "jdbc:sqlserver://localhost\\SQLEXPRESS;database=master;Trusted_Connection=True;"
			+ "integratedSecurity=true";
	*/
	public static List<String> imenaTablica = new ArrayList<>();

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Connection connection = null;

		Properties props = new Properties();

		try {
			InputStream is = sce.getServletContext().getResourceAsStream(DB_CON_STRING);
			props.load(is);
		} catch (Exception e) {
		}
		
		url = props.getProperty("db.conStr");
		
		try {
			connection = DriverManager.getConnection(new String(url.getBytes("UTF-8"), "UTF-8"));

			String id = " int IDENTITY(0, 1) primary key";
			String str = " varchar(150) not null";

			String createIzlozak = "CREATE TABLE Izlozak (sifIzlozak" + id + ", naziv" + str + ")";
			String createObjekt = "create table Objekt (sifObjekt" + id + ", naziv" + str + ")";
			String createAudio = "create table AudioZapis (sifAudio" + id + ", brojReprodukcija int " + ", relAdr" + str + ")";
			String createOpis = "create table Opis (sifOpis" + id + ", relAdr" + str + ")";
			String createKorisnik = "create table Korisnik (sifKorisnik" + id + ", ime" + str + ", prezime" + str
					+ ", email" + str + ", username" + str + ", lozinka" + str + ", potvrda" + str + ")";
			String createAdmin = "create table Administrator (sifAdmin" + id + ", ime" + str + ", prezime" + str
					+ ", kontakt" + str + ", username" + str + ", lozinka" + str + ")";

			String createJePreuzeo = "create table jePreuzeo ("
					+ "sifKorisnik int foreign key references Korisnik(sifKorisnik) on delete cascade,"
					+ "sifIzlozak int foreign key references Izlozak(sifIzlozak) on delete cascade,"
					+ "kolikoPuta int,"
					+ "primary key(sifKorisnik, sifIzlozak))";
			String createIma = "create table ima (sifIzlozak int foreign key references Izlozak(sifIzlozak) on delete cascade, "
					+ "sifAudio int foreign key references AudioZapis(sifAudio) on delete cascade, primary key(sifIzlozak))";
			String createSeNalazi = "create table seNalazi (sifIzlozak int foreign key references Izlozak(sifIzlozak) on delete cascade, "
					+ "sifObjekt int foreign key references Objekt(sifObjekt) on delete cascade, primary key(sifIzlozak))";
			String createJeDodao = "create table jeDodao (sifIzlozak int foreign key references Izlozak(sifIzlozak) on delete cascade, "
					+ "sifAdmin int foreign key references Administrator(sifAdmin) on delete cascade, primary key(sifIzlozak))";
			String createjeOpisan = "create table jeOpisan (sifIzlozak int foreign key references Izlozak(sifIzlozak) on delete cascade, "
					+ "sifOpis int foreign key references Opis (sifOpis) on delete cascade, primary key(sifIzlozak))";
			
			String[] creates = { createIzlozak, createObjekt, createAudio, createOpis, createKorisnik, createAdmin,
					createJePreuzeo, createIma, createSeNalazi, createJeDodao, createjeOpisan };

			Statement statement = null;
		
			for (String sql : creates) {
				String imeTablice = sql.split("\\s+")[2];
				imenaTablica.add(imeTablice);
				DatabaseMetaData metaData = connection.getMetaData();
				
				ResultSet rez = metaData.getTables(null, null, imeTablice,new String[] {"TABLE"});
				if (rez.next() && !rez.next()) continue;
				
				try {
					statement = connection.createStatement();
					statement.executeUpdate(sql);
				} catch (SQLException sqle) {
					System.err.println("Neuspješno dodavanje tablice " + imeTablice);
					sqle.printStackTrace();
				} finally {
					statement.close();
				}

				if (!imeTablice.equals("Administrator"))
					continue;

				try {
					statement = connection.createStatement();
					statement.executeUpdate("insert into Administrator(ime, prezime, kontakt, username, lozinka) "
							+ "values ('Šef', 'Gazdinović', 'sef.gazdinovic@audiovodic.io', 'Master', '" + Util.convertIntoSHA("zinka") + "')");
					
				} catch (SQLException nemaVeze) {
					nemaVeze.printStackTrace();
				} finally {
					statement.close();
				}
				
			}
			
			try {
				statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM Administrator");
				while(resultSet.next()) {
					Administrator.BROJ_ADMINA++;
				}
				
				
			} catch (SQLException nemaVeze) {
				nemaVeze.printStackTrace();
			} finally {
				statement.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				System.err.println("Neuspješno zatvaranje");
			}
		}
	}
}
