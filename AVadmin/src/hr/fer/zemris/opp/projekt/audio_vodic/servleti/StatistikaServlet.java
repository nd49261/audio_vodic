package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Korisnik;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Statistika;

@WebServlet("/statistika")
public class StatistikaServlet extends HttpServlet{

	private static final long serialVersionUID = 325L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		if(admin == null) {
			req.getRequestDispatcher("/login").forward(req, resp);
			return;
		}
		
		Map<Integer, Integer> brojReprodukcija = Statistika.brojAudioReprodukcija();
		Map<Integer, Integer> brojKorisnika = Statistika.brojKorisnikaKojiSuReproduciraliAudio();
		Map<Integer, Integer> brojPreuzimanja = Statistika.brojPreuzimanjaIzlozaka();
		List<Integer> podatci = new ArrayList<>();
		
		for(int sifIzlozak : brojKorisnika.keySet()) {
			podatci.add(sifIzlozak);
			podatci.add(brojKorisnika.get(sifIzlozak));
			Integer brRepr = brojReprodukcija.get(sifIzlozak);
			if(brRepr == null) podatci.add(0);
			else podatci.add(brRepr);
			Integer brPreuz = brojPreuzimanja.get(sifIzlozak);
			if(brPreuz == null) podatci.add(0);
			else podatci.add(brPreuz);
		}
		
		req.setAttribute("podatci", podatci);
		req.getRequestDispatcher("/statistika.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String sifAudioZapisString = req.getParameter("sifAudioZapis");
		
		if(sifAudioZapisString == null) {
			resp.sendError(HttpServletResponse.SC_BAD_GATEWAY);
			return;
		}
		Integer sifAudioZapis = Integer.parseInt(sifAudioZapisString);
		
		new Korisnik().reproducirajAudioZapis(sifAudioZapis);
		
		return;
	}

}
