package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Vlasnik;

/**
 * Nije testirano!
 * 
 * 
 * @author Matija
 *
 */

@WebServlet("/")
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 325L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getSession().getAttribute("administrator") != null) {
			resp.sendRedirect(req.getContextPath() + "/admin/objekti");
		} else {
			req.getRequestDispatcher("loginAdmina.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = req.getParameter("username");
		String lozinka = req.getParameter("lozinka");
	
		Administrator admin = new Administrator(username, lozinka);
		if(admin.prijava()) {
			req.getSession().setAttribute("administrator", admin);
			if(admin.getSifAdmin() == 0) {
				req.getSession().setAttribute("vlasnik", new Vlasnik(admin));
			}
			
			resp.sendRedirect(req.getContextPath() + "/admin/objekti");
			//req.getRequestDispatcher("/admin/objekti").forward(req, resp);
		} else {
			req.setAttribute("error", "Prijava neuspješna.");
			req.getRequestDispatcher("loginAdmina.jsp").forward(req, resp);
		}
	}

}
