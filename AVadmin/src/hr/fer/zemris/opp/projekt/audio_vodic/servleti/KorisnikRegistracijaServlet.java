package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;

import hr.fer.zemris.opp.projekt.audio_vodic.form.KorisnikRegForm;
import hr.fer.zemris.opp.projekt.audio_vodic.model.NeregistriraniKorisnik;

@WebServlet("/korisnikRegistracija")
public class KorisnikRegistracijaServlet extends HttpServlet {
	private static final long serialVersionUID = 325L;
	private static final String CONFIG_FILE = "/WEB-INF/mail.properties";

//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//
//			resp.getOutputStream().println("Nešto.");
//
//		resp.getOutputStream().flush();
//		resp.getOutputStream().close();
//	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		KorisnikRegForm regForm = new KorisnikRegForm(req.getParameter("ime"), req.getParameter("prezime"),
				req.getParameter("email"), req.getParameter("username"), req.getParameter("lozinka"));

		if (regForm.provjeri()) {
			registracija(regForm, resp.getOutputStream(), req.getRequestURL().substring(0, req.getRequestURL().lastIndexOf("korisnikRegistracija")));
		} else {
			resp.getOutputStream().println(regForm.getErrorPoruka());
		}

		resp.getOutputStream().flush();
		resp.getOutputStream().close();
	}

	private void registracija(KorisnikRegForm form, ServletOutputStream stream, String url) throws IOException {
		/*Connection veza = null;
		try {
			veza = DriverManager.getConnection(Inicijalizacija.url);
			PreparedStatement pst = veza.prepareStatement("select * from Korisnik where username = ?");
			pst.setString(1, form.getUsername());
			ResultSet res = pst.executeQuery();
			
			if (res.isBeforeFirst()) {
				stream.println("Username se već koristi");
				pst.close();
				veza.close();
				return;
			}
			pst.close();

			String kljuc = generateKey();
			pst = veza.prepareStatement(
					"insert into Korisnik (ime, prezime, email, username, lozinka, potvrda) values (?, ?, ?, ?, ?, ?)");
			pst.setString(1, form.getIme());
			pst.setString(2, form.getPrezime());
			pst.setString(3, form.getEmail());
			pst.setString(4, form.getUsername());
			pst.setString(5, Util.convertIntoSHA(form.getLozinka()));
			pst.setString(6, kljuc);
			pst.executeUpdate();

			pst.close();
			veza.close();

			posaljiMail(kljuc, form.getEmail(), url, stream);
		} catch (Exception e) {

			stream.println("Greška pri povezivanju s bazom!");
			// e.printStackTrace(new PrintStream(stream));

			return;
		}

		stream.println("OK");*/
		
		
		NeregistriraniKorisnik neRegKorisnik = new NeregistriraniKorisnik(form);
		stream.println(neRegKorisnik.registrirajSe());
		if(neRegKorisnik.getKljuc() != null) posaljiMail(neRegKorisnik.getKljuc(), form.getEmail(), url, stream);
	}

	private boolean posaljiMail(String kljuc, String mailAdresa, String url, ServletOutputStream stream) throws IOException {
		Email email = new SimpleEmail();
		Properties props = new Properties();

		try {
			InputStream is = getServletContext().getResourceAsStream(CONFIG_FILE);
			props.load(is);
		} catch (Exception e) {
			return false;
		}

		try{
		email.setHostName(props.getProperty("mail.HostName"));
		email.setSmtpPort(Integer.parseInt(props.getProperty("mail.SmtpPort")));
		email.setAuthenticator(new DefaultAuthenticator(props.getProperty("mail.username"), props.getProperty("mail.password")));
		email.setSSLOnConnect(true);
		email.setFrom(props.getProperty("mail.From"));
		email.setSubject(props.getProperty("mail.Subject"));
		email.setMsg(props.getProperty("mail.Msg") + url + "potvrda/" + kljuc);
		email.addTo(mailAdresa);
		email.send();
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}

	
}
