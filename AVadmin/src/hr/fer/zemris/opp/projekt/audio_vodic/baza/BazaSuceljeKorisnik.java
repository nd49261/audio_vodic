package hr.fer.zemris.opp.projekt.audio_vodic.baza;

import java.sql.SQLException;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Izlozak;

/**
 * @author Matija Kušec
 * 
 * Sučelje kojim administrator komunicira s bazom podataka.
 *
 */
public interface BazaSuceljeKorisnik {
	
	/**
	 * Metoda koja uspostavlja vezu s bazom podataka.
	 * 
	 * @throws SQLException
	 */
	public void uspostaviVezu() throws SQLException;

	/**
	 * Metoda koja vraća izločak za priloženu šifru.
	 * 
	 * @param sifIzlozak - sifra traženog izloška
	 * @return traženi izložak
	 * @throws SQLException
	 */
	public Izlozak dohvatiIzlozak(int sifIzlozak) throws SQLException;
	
	public void reproducirajAudioZapis(Integer sifAudio);

}
