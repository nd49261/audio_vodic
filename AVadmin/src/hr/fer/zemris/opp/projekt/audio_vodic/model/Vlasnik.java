package hr.fer.zemris.opp.projekt.audio_vodic.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import hr.fer.zemris.opp.projekt.audio_vodic.crypt.Util;

public class Vlasnik extends Administrator {

	public Vlasnik(int sifAdmin, String ime, String prezime, String kontakt, String username, String lozinka) {
		super(sifAdmin, ime, prezime, kontakt, username, lozinka);
	}
	
	public Vlasnik(Administrator admin) {
		super(admin.getSifAdmin(), admin.getIme(), admin.getPrezime(), admin.getKontakt(), admin.getUsername(), admin.getLozinka());
	}

	public boolean dodajAdmina(String username, String lozinka) {
		if (Administrator.BROJ_ADMINA >= 5) return false;
		
		try {
			uspostaviVezu();
			
//			Statement statement = veza.createStatement();
//			statement.executeQuery(
//					"SELECT * FROM Administrator WHERE username =" + username);
//			
			PreparedStatement statement = veza.prepareStatement("SELECT * FROM Administrator WHERE username =?");
			statement.setString(1, username);
			
			ResultSet resultSet = statement.executeQuery();
			boolean ima = resultSet.next();
			statement.close();
			
			if (!ima) {
				PreparedStatement pstat = veza.prepareStatement("INSERT INTO Administrator(ime, prezime, kontakt, username, lozinka) "
						+ "values ('Ime', 'Prezime', 'ovdje.email@audiovodic325.io', ?, ?)");
				
				pstat.setString(1, username);
				pstat.setString(2, Util.convertIntoSHA(lozinka));
				
				pstat.executeUpdate();
				pstat.close();
			}

			veza.close();
			
			if (ima) return false;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
		
		Administrator.BROJ_ADMINA++;
		return true;		
	}
	
}
