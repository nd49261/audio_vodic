package hr.fer.zemris.opp.projekt.audio_vodic.crypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class that provides utilities, conversion from hexadecimal to byte and vice versa and a SHA-1 conversion.
 * 
 * @author Matija Kušec
 *
 */
public class Util {
	
	/**
	 * Converts the given text in hexadecimal format to an array of bytes.
	 * 
	 * @param keyText The given text
	 * @return byte array
	 * 
	 * @throws IllegalArgumentException if the input text is malformed
	 */
	public static byte[] hextobyte(String keyText) {
		int len = keyText.length();
		if ((len % 2) == 1) {
			throw new IllegalArgumentException("Odd length of key text");
		}

		byte[] ret = new byte[len >> 1];
		for (int i = 0; i < len; i += 2) {
			char c1 = keyText.toLowerCase().charAt(i), c2 = keyText.toLowerCase().charAt(i + 1);
			if (!Character.isDigit(c1) && (c1 < 'a' || c1 > 'f') || !Character.isDigit(c2) && (c2 < 'a' || c2 > 'f')) {
				throw new IllegalArgumentException("Text is not in hexadecimal format");
			}

			ret[i >> 1] = (byte) (Character.digit(c1, 16) << 4 | Character.digit(c2, 16));
		}

		return ret;
	}

	/**
	 * Converts a byte array to a string in hexadecimal format
	 * 
	 * @param bytearray the byte array
	 * @return The string
	 */
	public static String bytetohex(byte[] bytearray) {
		StringBuilder ret = new StringBuilder();
		for (int i = 0; i < bytearray.length; i++) {
			byte x = bytearray[i];
			ret.append(Character.forDigit((x >> 4) & 15, 16));
			ret.append(Character.forDigit(x & 15, 16));
		}

		return ret.toString();
	}
	
	/**
	 * Converts a string into a crypted string using the SHA-1 algorithm.
	 * 
	 * @param s the input string
	 * @return the crypted string
	 */
	public static String convertIntoSHA(String s) {
		MessageDigest mDigest = null;
		try {
			mDigest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException ignorable) {}
		
		mDigest.update(s.getBytes());
		
		return bytetohex(mDigest.digest());
	}
}
