package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;	
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.form.KorisnikRegForm;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator;

@WebServlet("/urediPodatke")
public class UrediPodatke extends HttpServlet{

	private static final long serialVersionUID = 325L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.getRequestDispatcher("/login").forward(req, resp);
		}
		
		req.setAttribute("ime", admin.getIme());
		req.setAttribute("prezime", admin.getPrezime());
		req.setAttribute("kontakt", admin.getKontakt());
		req.setAttribute("username", admin.getUsername());
		
		resp.sendRedirect(req.getContextPath() + "/urediPodatke.jsp");
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String ime = req.getParameter("ime");
		String prezime = req.getParameter("prezime");
		String kontakt = req.getParameter("kontakt");
		String username = req.getParameter("username");
		String lozinka = req.getParameter("lozinka");
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		KorisnikRegForm form = new KorisnikRegForm(ime, prezime, kontakt, username, lozinka);
		Administrator promjenjeniAdmin = new Administrator(admin.getSifAdmin(), ime,
				prezime, kontakt, username, lozinka);
		
		String error = null;
		if(form.provjeri()) {
			if(promjenjeniAdmin.spremiSvojePodatke()) {
				req.getSession().setAttribute("administrator", promjenjeniAdmin);
				resp.sendRedirect(req.getContextPath() + "/admin/objekti");
				return;
			} else {
				error = "Username se već koristi";
			}
		} else {
			error = form.getErrorPoruka();
		}
		if(error != null) {
			req.setAttribute("error", error);
			req.getRequestDispatcher("/urediPodatke.jsp").forward(req, resp);
		}
	}
}
