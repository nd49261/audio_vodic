package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Izlozak;
import hr.fer.zemris.opp.projekt.audio_vodic.model.NeregistriraniKorisnik;
import hr.fer.zemris.opp.projekt.audio_vodic.model.RegistriraniKorisnik;

@WebServlet("/dohvatiIzlozak")
public class DohvatiIzlozakServlet extends HttpServlet{
	private static final long serialVersionUID = 325L;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String QRKOD = (String) req.getParameter("qrkod");
		String audio = (String) req.getParameter("audio");
		String sifKorisnikString = (String) req.getParameter("sifKorisnik");
		Administrator admin = (Administrator)req.getSession().getAttribute("administrator");
		int sifKorisnik = 0;
		
		if(sifKorisnikString != null) {
			try {
				sifKorisnik = Integer.parseInt(sifKorisnikString);
			} catch(NumberFormatException nfe) {
				resp.sendError(HttpServletResponse.SC_BAD_GATEWAY);
				return;
			}
		}
		
		resp.setCharacterEncoding("UTF-8");
		if(audio == null) resp.setContentType("text/html; charset=UTF-8");
		
		ServletOutputStream os = resp.getOutputStream();
	
		if(QRKOD == null) {

			os.println("NOTOK");
			os.flush();
			os.close();
			return;
		}
		
		
		
		StringBuilder integer = new StringBuilder();
		for(char c : QRKOD.toCharArray()) {
			if(Character.isDigit(c)) integer.append(c);
		}
		
		int sifIzl = Integer.parseInt(integer.toString());
		Izlozak izlozak = null;
		try {
			if(sifKorisnikString != null) {
				izlozak = new RegistriraniKorisnik(sifKorisnik).dohvatiIzlozak(sifIzl);
			} else if (admin!=null){
				izlozak = admin.dohvatiIzlozak(sifIzl);
			} else {
				izlozak = new NeregistriraniKorisnik().dohvatiIzlozak(sifIzl);
			}
		} catch (SQLException e) {

			os.println("NOTOK");
			os.flush();
			os.close();
			e.printStackTrace();
			return;
		}

		if(audio != null) {
			resp.setContentType("audio/mpeg");
			
			Path opis = Paths.get(izlozak.getAudioZapis().trim());
			
			byte[] bytes = Files.readAllBytes(opis);
			resp.setContentLength((int) bytes.length);
			os.write(bytes);
			
			os.flush();
			os.close();
			return;
		}
			
		if(izlozak == null) {
			os.println("NOTOK");
			os.flush();
			os.close();
			return;
		}
		os.println(izlozak.getNaziv());
		
		//Oprez: zasad su sifAudioZapis i sifIzlozak uvijek isti, ako se to promijeni sljedeća linija nije valjana
		os.println(izlozak.getSifIzlozak()); 
		
		Path opis = Paths.get(izlozak.getOpis().trim());
		List<String> lines = Files.readAllLines(opis);
		lines.forEach(line -> {
			try {
				os.println(line);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		os.flush();
		os.close();
	}
}
