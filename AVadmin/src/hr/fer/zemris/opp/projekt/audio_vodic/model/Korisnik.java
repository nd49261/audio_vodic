package hr.fer.zemris.opp.projekt.audio_vodic.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import hr.fer.zemris.opp.projekt.audio_vodic.baza.BazaSuceljeKorisnik;
import hr.fer.zemris.opp.projekt.audio_vodic.baza.Inicijalizacija;

public class Korisnik implements BazaSuceljeKorisnik {
	protected String ime;
	protected String prezime;
	protected String email;
	protected String username;
	protected String lozinka;
	
	protected Connection veza;

	public String getIme() {
		return ime;
	}


	public String getPrezime() {
		return prezime;
	}


	public String getEmail() {
		return email;
	}


	public String getUsername() {
		return username;
	}


	public String getLozinka() {
		return lozinka;
	}

	public Korisnik() {
	}
	
	@Override
	public void uspostaviVezu() {
		try {
			if(veza != null && !veza.isClosed()) return;
			veza = DriverManager.getConnection(Inicijalizacija.url);
		} catch (SQLException e) {
			System.err.println("Korisnik ne može dobiti vezu.");
		}
	}


	@Override
	public Izlozak dohvatiIzlozak(int sifIzlozak) throws SQLException {
		Izlozak izlozak = new Administrator("netko", "sifra").dohvatiIzlozak(sifIzlozak);
		return izlozak;
	}

	@Override
	public void reproducirajAudioZapis(Integer sifAudio) {
		Statistika.korisnikReproduciraAudio(sifAudio);
	}
	
	
}
