package hr.fer.zemris.opp.projekt.audio_vodic.model;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import hr.fer.zemris.opp.projekt.audio_vodic.baza.Inicijalizacija;
import hr.fer.zemris.opp.projekt.audio_vodic.crypt.Util;

public class RegistriraniKorisnik extends Korisnik {
	private int sifKorisnik;
	
	public RegistriraniKorisnik(int sifKorisnik) throws SQLException {
		uspostaviVezu();
		Statement st = veza.createStatement();
		ResultSet res = st.executeQuery(
				"SELECT kor.ime, kor.prezime, kor.email, kor.username, kor.lozinka "
			  + "FROM Korisnik as kor "
			  + "WHERE kor.sifKorisnik =" + sifKorisnik);
		
		boolean ima = res.next();
		if(ima) {
			this.sifKorisnik = sifKorisnik;
			this.ime = res.getString(1);
			this.prezime = res.getString(2);
			this.email = res.getString(3);
			this.username = res.getString(4);
			this.lozinka = res.getString(5);
		} else {
			this.sifKorisnik = -1;
		}
		st.close();
		veza.close();
	}
	

	public RegistriraniKorisnik() {
		super();
	}


	@Override
	public Izlozak dohvatiIzlozak(int sifIzlozak) throws SQLException {
		Izlozak izlozak = super.dohvatiIzlozak(sifIzlozak);
		Statistika.preuzetiPodatciOIzlosku(sifKorisnik, sifIzlozak);
		
		return izlozak;
	}

	public String prijava(String username, String lozinka) {
		uspostaviVezu();
		try {
			veza = DriverManager.getConnection(Inicijalizacija.url);
			Statement st = veza.createStatement();
			ResultSet res = st.executeQuery("select * from Korisnik where username = '" + username + "' and lozinka ='"
			+ Util.convertIntoSHA(lozinka) + "'");
			
			if(!res.next()) return "NEMA";
			String sifKorisnik = res.getString(1);
			String potvrda = res.getString(7);
			
			st.close();
			veza.close();

			if (!potvrda.equals("DA")) return "POTVRDA";
			
			return sifKorisnik;
		} catch(Exception e) {
			return "NOTOK";
		}
	}
}
