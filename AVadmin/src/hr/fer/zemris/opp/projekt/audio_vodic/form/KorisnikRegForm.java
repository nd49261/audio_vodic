package hr.fer.zemris.opp.projekt.audio_vodic.form;

public class KorisnikRegForm {
	private String ime;
	private String prezime;
	private String email;
	private String username;
	private String lozinka;
	private Boolean ispravan;
	private String errorPoruka;
	
	public KorisnikRegForm(String ime, String prezime, String email, String username, String lozinka) {
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.username = username;
		this.lozinka = lozinka;
	}
	
	public String getErrorPoruka() {
		return errorPoruka;
	}
	
	public boolean provjeri() {
		if(ispravan != null) return ispravan;
		
		if(ime == null || ime.length() < 2) {
			errorPoruka = "Ime je prekratko";
			return ispravan = false;
		}
		
		if(prezime == null || ime.length() < 2) {
			errorPoruka = "Prezime je prekratko";
			return ispravan = false;
		}
		
		if(email == null || email.length() < 2) {
			errorPoruka = "Email je prekratak";
			return ispravan = false;
		}
		
		if(username == null || username.length() < 2) {
			errorPoruka = "Username je prekratak";
			return ispravan = false;
		}
		
		
		for(char c : ime.toCharArray()) {
			if(!Character.isAlphabetic(c)) {
				errorPoruka = "Ime se mora sastojati samo od slova!";
				return ispravan = false;
			}
		}
		
		for(char c : prezime.toCharArray()) {
			if(!Character.isAlphabetic(c)) {
				errorPoruka = "Prezime se mora sastojati samo od slova!";
				return ispravan = false;
			}
		}
		
		if (!email.contains("@")) {
			errorPoruka = "Email nema '@'!";
			return ispravan = false;
		}
		
		if(lozinka.length() < 5) {
			errorPoruka = "Loznika mora imati najmanje 5 znakova";
			return ispravan = false;
		}
		
		return ispravan = true;
	}

	public String getIme() {
		return ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getLozinka() {
		return lozinka;
	}

	public Boolean getIspravan() {
		return ispravan;
	}
}
