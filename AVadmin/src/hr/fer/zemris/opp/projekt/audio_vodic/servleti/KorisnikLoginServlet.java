package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.projekt.audio_vodic.model.RegistriraniKorisnik;


@WebServlet("/korisnikLogin")
public class KorisnikLoginServlet extends HttpServlet {

	private static final long serialVersionUID = 325L;

//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		resp.getOutputStream().println(login("malikorisni325", "malalozinka"));
//		resp.getOutputStream().flush();
//		resp.getOutputStream().close();
//	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = req.getParameter("username");
		String lozinka = req.getParameter("lozinka");
		
		resp.getOutputStream().println(login(username, lozinka));
		
		resp.getOutputStream().flush();
		resp.getOutputStream().close();
	}
	
	private String login(String username, String lozinka) {
		/*Connection veza = null;
		try {
			veza = DriverManager.getConnection(Inicijalizacija.url);
			Statement st = veza.createStatement();
			ResultSet res = st.executeQuery("select * from Korisnik where username = '" + username + "' and lozinka ='"
			+ Util.convertIntoSHA(lozinka) + "'");
			
			if(!res.next()) return "NEMA";
			String sifKorisnik = res.getString(1);
			String potvrda = res.getString(7);
			
			st.close();
			veza.close();

			if (!potvrda.equals("DA")) return "POTVRDA";
			
			return sifKorisnik;
		} catch(Exception e) {
			return "NOTOK"+ e.getMessage();
		}*/
		
		return new RegistriraniKorisnik().prijava(username, lozinka);
	}
}
