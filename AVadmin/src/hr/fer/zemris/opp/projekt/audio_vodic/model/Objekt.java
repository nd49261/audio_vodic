package hr.fer.zemris.opp.projekt.audio_vodic.model;

import java.util.ArrayList;
import java.util.List;

public class Objekt {
	
	private int sifObjekt;
	private String naziv;
	private List<Izlozak> izlosci;
	
	public Objekt(int sifObjekt,String naziv) {
		izlosci = new ArrayList<>();
		this.naziv = naziv;
		this.sifObjekt = sifObjekt;
	}

	public int getSifObjekt() {
		return sifObjekt;
	}

	public void setSifObjekt(int sifObjekt) {
		this.sifObjekt = sifObjekt;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<Izlozak> getIzlosci() {
		return izlosci;
	}

	public void dodajIzlozak(Izlozak izlozak) {
		izlosci.add(izlozak);
	}

	@Override
	public String toString() {
		return "Objekt naziv: " + naziv;
	}
}
