package hr.fer.zemris.opp.projekt.audio_vodic.model;

public class Izlozak {
	
	private int sifIzlozak;
	private String naziv;
	private String audioZapis;
	private String opis;
	
	public Izlozak(int sifIzlozak, String naziv, String audioZapis, String opis) {
		this.sifIzlozak = sifIzlozak;
		this.naziv = naziv;
		this.audioZapis = audioZapis;
		this.opis = opis;
	}
	
	public int getSifIzlozak() {
		return sifIzlozak;
	}
	
	public void setSifIzlozak(int sifIzlozak) {
		this.sifIzlozak = sifIzlozak;
	}
	
	public String getNaziv() {
		return naziv;
	}
	
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	
	public String getAudioZapis() {
		return audioZapis;
	}

	public void setAudioZapis(String audioZapis) {
		this.audioZapis = audioZapis;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + sifIzlozak;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Izlozak other = (Izlozak) obj;
		if (sifIzlozak != other.sifIzlozak)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Izlozak naziv: " + naziv + " , audioZapis: " + audioZapis + ", opis: " + opis;
	}
	
}
