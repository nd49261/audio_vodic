package hr.fer.zemris.opp.projekt.audio_vodic.servleti;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import hr.fer.zemris.opp.projekt.audio_vodic.model.Administrator;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Izlozak;
import hr.fer.zemris.opp.projekt.audio_vodic.model.Objekt;
import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Header;

/**
 * Nije testirano!
 * 
 * 
 * @author Matija
 *
 */

@WebServlet("/admin/*")
@MultipartConfig(location="/res/tmp", fileSizeThreshold=1024*1024, 
maxFileSize=1024*1024*11, maxRequestSize=1024*1024*5*5)
public class AdminServlet extends HttpServlet{
	private static final long serialVersionUID = 325L;
	public static final int MAX_TRAJANJE = 180;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		if(admin == null) {
			req.getRequestDispatcher("/login").forward(req, resp);
			return;
		}
		
		String[] parameters = req.getPathInfo().substring(1).split("[/]");
		
		if(parameters.length >= 1 && parameters[0].equals("objekti")) {
			if(parameters.length >= 2) {
				//"admin/objekti/0"
				Objekt objekt;
				try {
					objekt = admin.dohvatiObjekt(Integer.parseInt(parameters[1]));
				} catch (NumberFormatException | SQLException e) {
					e.printStackTrace();
					resp.sendError(HttpServletResponse.SC_BAD_GATEWAY);
					return;
				}
				
				req.setAttribute("sifra", objekt.getSifObjekt());
				req.setAttribute("naziv", objekt.getNaziv());
				
				
				if(parameters.length == 2) {
					try {
						objekt.getIzlosci().forEach(iz -> {
							iz.setAudioZapis(req.getContextPath() + "/" + iz.getAudioZapis());
							iz.setOpis(req.getContextPath() + "/" + iz.getOpis());
						});
						req.setAttribute("izlosci", objekt.getIzlosci());
						req.getRequestDispatcher("/prikazIzlozaka.jsp").forward(req, resp);
					} catch(Exception e) {
						e.printStackTrace();
						resp.sendError(HttpServletResponse.SC_BAD_GATEWAY);
					}
					return;
				}
				
				Izlozak izlozak = null;
				try {
					int sifraIzloska = Integer.parseInt(parameters[2]);
					for(Izlozak izl : objekt.getIzlosci()) {
						if(izl.getSifIzlozak() == sifraIzloska) {
							izlozak = izl;
							break;
						}
					}
				} catch(NumberFormatException e) {
					e.printStackTrace();
					resp.sendError(HttpServletResponse.SC_BAD_GATEWAY);
					return;
				}
				
				//"admin/objekti/0/2"
				if(parameters.length == 3) {
					req.setAttribute("izlozak", izlozak);
					req.getRequestDispatcher("/izmijeniIzlozak.jsp").forward(req, resp);
					return;
				}
				
				//"admin/objekti/0/2/del"
				if(parameters.length == 4 && parameters[3].equals("del")) {
					try {
						Files.deleteIfExists(Paths.get(izlozak.getOpis()));
						Files.deleteIfExists(Paths.get(izlozak.getAudioZapis()));
						Files.deleteIfExists(Paths.get(izlozak.getOpis().substring(0, izlozak.getOpis().lastIndexOf("/"))));
						admin.izbrisiIzlozak(izlozak.getSifIzlozak());
					} catch(IOException | SecurityException | SQLException e) {
						e.printStackTrace();
						resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
				}
			}
			// "admin/objekti"
			try {
				List<Objekt> objekti = admin.dohvatiSveObjekte();
				req.setAttribute("objekti", objekti);
				req.getRequestDispatcher("/prikazObjekata.jsp").forward(req, resp);
			} catch (SQLException e) {
				resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] parameters = req.getPathInfo().substring(1).split("[/]");
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		if(admin == null) {
			resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		
		if(parameters.length >= 1 && parameters[0].equals("objekti")) {
		
			
			if(parameters.length >= 2) {
				
				int sifObjekt = Integer.parseInt(parameters[1]);
				
				OutputStream out = null;
				InputStream in = null;
				try {
					Izlozak izlozak = null;
					if(parameters.length == 2) {
						izlozak = new Izlozak(-1, req.getParameter("naziv"), "", "");
					} else if(parameters.length == 3) {
						int sifIzlozak = Integer.parseInt(parameters[2]);
						izlozak = admin.dohvatiIzlozak(sifIzlozak);
						if(izlozak == null) {
							resp.sendError(HttpServletResponse.SC_BAD_GATEWAY);
							return;
						}
						izlozak.setNaziv(req.getParameter("naziv"));
					}
					
					admin.pohraniIzlozak(izlozak, sifObjekt);
					
					byte[] bytes = req.getParameter("opis").getBytes("UTF-8");
					Path path = Paths.get(izlozak.getOpis().trim());
					
					try {
						String s = izlozak.getOpis().substring(0, izlozak.getOpis().lastIndexOf('/'));
						Files.createDirectories(Paths.get(s));
					} catch (FileAlreadyExistsException ex) {
						System.err.println("File already exists");
					} catch (SecurityException ex) {
						System.err.println("Security issues");
					} catch (IOException ex) {
						System.err.println("Input/Ouput error");
					}
					
					
					if(!Files.exists(path)) Files.createFile(path);
					
					
					Files.write(path, bytes);
					
					String error = null;
					
					Part filePart = req.getPart("audio");
					if(filePart != null && filePart.getSize() > 1) {
				        in = filePart.getInputStream();
				      //  in.mark(1024 * 1024 * 11);
				        
				        try {
				        	int trajanje;
				        	try {
					        	AudioInputStream fis = AudioSystem.getAudioInputStream(in);
					        	AudioInputStream ais = AudioSystem.getAudioInputStream(
								             AudioFormat.Encoding.PCM_SIGNED, fis);
					        	
					        	AudioFormat af = ais.getFormat();
						        long frames = ais.getFrameLength();
						        trajanje = (int) ((frames + 0.0) / af.getFrameRate());
				        	} catch(UnsupportedAudioFileException uafe) {
				        		Header head;
				        		Bitstream bitstream = new Bitstream(in);
				        		try {
									head = bitstream.readFrame();
								} catch (BitstreamException e) {
									throw new UnsupportedAudioFileException();
								}
				        		int len = in.available();
				        		trajanje = (int) (head.total_ms(len) / 1000);
				        	}
				        	
							if(trajanje > MAX_TRAJANJE) {
								error = "Audio zapis traje " + trajanje + " s,"
										+ " a najveće dopušteno trajanje je " + MAX_TRAJANJE + " s";
							}
							 
						} catch (UnsupportedAudioFileException e) {
							error = "Audio zapis nije dopuštenog formata";
						}
				        
				        // in.reset();
				       
				        if(error == null) {
					        out = Files.newOutputStream(Paths.get(izlozak.getAudioZapis()));
					        int read = 0;
					        bytes = new byte[1024];
	
					        while ((read = in.read(bytes)) != -1) {
					            out.write(bytes, 0, read);
					        }
				        }
					} else if(parameters.length == 2) {
						error = "Audio zapis nije postavljen";
					}
					
					if(error != null) {
			        	req.setAttribute("greska", error);
			        	req.setAttribute("izlozak", izlozak);
			        	req.setAttribute("sifra", sifObjekt);
			        	req.setAttribute("naziv", admin.dohvatiObjekt(sifObjekt).getNaziv());
			        	req.getRequestDispatcher("/izmijeniIzlozak.jsp").forward(req, resp);
			        	return;
			        }
			        
				} catch (SQLException e) {
					resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
					e.printStackTrace();
				}finally {
					
					if(out != null) out.close();
			        if(in != null) in.close();
					
				}
				
				resp.sendRedirect(req.getContextPath() + "/admin/objekti/" + sifObjekt);
				
				return;
			}
			
			String naziv = req.getParameter("naziv");
			Objekt objekt = new Objekt(-1, naziv);
			try {
				admin.pohraniObjekt(objekt);
			} catch (SQLException e) {
				resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				e.printStackTrace();
			}
			
			resp.sendRedirect(req.getContextPath() + "/admin/objekti");
			return;
		}
	}

}
