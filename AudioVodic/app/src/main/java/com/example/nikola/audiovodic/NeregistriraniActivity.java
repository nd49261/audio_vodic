package com.example.nikola.audiovodic;


import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nikola.audiovodic.MediaPlayer.MediaPlayerActivity;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.example.nikola.audiovodic.barcode.BarcodeCaptureActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NeregistriraniActivity extends AppCompatActivity {
    private static final String LOG_TAG = NeregistriraniActivity.class.getSimpleName();
    private static final int BARCODE_READER_REQUEST_CODE = 1;
    public static String qrcode1;

    private ProgressBar pb;
    private TextView mResultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pb = (ProgressBar) findViewById(R.id.neregKorisnik_progress);

        if (isInternetAvailable()) {
            Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
            startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
        }

        Button scanBarcodeButton = (Button) findViewById(R.id.scan_barcode_button);
        scanBarcodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInternetAvailable()) {
                    Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
                    startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
                } else
                    Toast.makeText(NeregistriraniActivity.this, R.string.no_internet, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    Point[] p = barcode.cornerPoints;
                    pb.setVisibility(View.VISIBLE);
                    qrcode1 = barcode.displayValue.replaceAll("\\s+", "");
                    ;
                    GetTekstN getN = new GetTekstN();
                    getN.execute((Void) null);
                } else mResultTextView.setText(R.string.no_barcode_captured);
            }
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public class GetTekstN extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected List<String> doInBackground(Void... voids) {
            List<String> odg = new ArrayList<>();
            try {
                URL url = new URL("https://avadmin.azurewebsites.net/dohvatiIzlozak?qrkod=" + qrcode1);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    odg.add(line);
                }

                in.close();
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL: " + e.getMessage());
            } catch (IOException e) {
                System.out.println("I/O Error: " + e.getMessage());
            }
            return odg;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            super.onPostExecute(strings);
            pb.setVisibility(View.GONE);
            if (strings.size() >= 3) {
                Intent toIzlozakN = new Intent(NeregistriraniActivity.this, MediaPlayerActivity.class);
                toIzlozakN.putStringArrayListExtra("tekst1", (ArrayList<String>) strings);
                startActivity(toIzlozakN);
            }
            else Toast.makeText(NeregistriraniActivity.this,R.string.greska_izlozaak, Toast.LENGTH_LONG).show();
        }
    }
}
