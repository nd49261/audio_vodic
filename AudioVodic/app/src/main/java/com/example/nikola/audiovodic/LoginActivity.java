package com.example.nikola.audiovodic;

/**
 * Created by Nikola on 4.1.2018..
 */



import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends AppCompatActivity  {
    public static boolean reg;
    // UI references.
    private ProgressBar pbr;
    private Button regbuton;
    private  Button neregbut;
    private EditText mUsernameView;
    private EditText mPasswordView;

    public static String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        pbr = (ProgressBar) findViewById(R.id.login_progress);
        mUsernameView = (EditText) findViewById(R.id.username);

        mPasswordView = (EditText) findViewById(R.id.password);

        regbuton= (Button) findViewById(R.id.regbutton);
        regbuton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int2 = new Intent(LoginActivity.this, RegistracijaActivity.class);
                startActivity(int2);
            }
        });
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetAvailable()) {
                    pbr.setVisibility(View.VISIBLE);
                    reg = true;
                    UserLoginTask usr = new UserLoginTask();
                    usr.execute((Void) null);
                }
                else Toast.makeText(LoginActivity.this,R.string.no_internet,Toast.LENGTH_LONG).show();
            }
        });
        neregbut = (Button) findViewById(R.id.neregbutton);
        neregbut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetAvailable()) {
                    reg = false;
                    Intent neReg = new Intent(LoginActivity.this, NeregistriraniActivity.class);
                    startActivity(neReg);
                }
                else Toast.makeText(LoginActivity.this,R.string.no_internet,Toast.LENGTH_LONG).show();
            }
        });


    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */

    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            URL url = null;
            try {
                url = new URL("https://avadmin.azurewebsites.net/korisnikLogin");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            String s = null;
            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("username", mUsernameView.getText().toString())
                        .appendQueryParameter("lozinka", mPasswordView.getText().toString());

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(builder.build().getEncodedQuery());
                writer.flush();
                writer.close();
                os.close();

                connection.connect();

                InputStream is = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                s = br.readLine();
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return s;

        }

        protected void onPostExecute(String s) {
            pbr.setVisibility(View.GONE);

            if (s.equals("POTVRDA")) {
                Toast.makeText(LoginActivity.this, R.string.potvrdite_mail, Toast.LENGTH_LONG).show();
                return;
            }else if (s.equals("NEMA")) {
                Toast.makeText(LoginActivity.this, R.string.neispravni_podatci, Toast.LENGTH_LONG).show();
                return;
            }else if (s.equals("NOTOK")) {
                Toast.makeText(LoginActivity.this, R.string.greska_server, Toast.LENGTH_LONG).show();
                return;
            } else if (isInteger(s)) {
                id = s;
                Toast.makeText(LoginActivity.this, "Uspješna prijava!", Toast.LENGTH_SHORT).show();
                Intent goToMain = new Intent(LoginActivity.this, KorisnikActivity.class);
                startActivity(goToMain);
                return;
            }
            Toast.makeText(LoginActivity.this, s, Toast.LENGTH_SHORT).show();
        }
        public boolean isInteger(String s) {
            try {
                Integer.parseInt(s);
            } catch(NumberFormatException e) {
                return false;
            } catch(NullPointerException e) {
                return false;
            }
            return true;
        }
    }
}

