package com.example.nikola.audiovodic.MediaPlayer;

/**
 * Created by Nikola on 6.1.2018..
 */

public interface PlayerAdapter {

    void loadMedia();

    void release();

    boolean isPlaying();

    void play();

    void reset();

    void pause();

    void initializeProgressCallback();

    void seekTo(int position);
}
