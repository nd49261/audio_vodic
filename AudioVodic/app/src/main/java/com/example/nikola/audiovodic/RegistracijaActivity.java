package com.example.nikola.audiovodic;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RegistracijaActivity extends AppCompatActivity {

    private ProgressBar pb;
    private EditText email;
    private EditText ime;
    private EditText prezime;
    private EditText username;
    private EditText lozinka;
    private Button regBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracija);

        pb = (ProgressBar) findViewById(R.id.registracija_progress);
        regBtn = (Button) findViewById(R.id.registracija_btt);
        email = (EditText) findViewById(R.id.registracija_email);
        ime = (EditText) findViewById(R.id.registracija_ime);
        prezime = (EditText)  findViewById(R.id.registracija_prezime);
        username = (EditText)  findViewById(R.id.registracija_username);
        lozinka = (EditText)  findViewById(R.id.registracija_lozinka);

        regBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isInternetAvailable()) {
                    pb.setVisibility(View.VISIBLE);
                    RegistracijaTask task = new RegistracijaTask();
                    task.execute((Void) null);
                }
                else Toast.makeText(RegistracijaActivity.this,R.string.no_internet,Toast.LENGTH_LONG).show();
            }
        });
    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public class RegistracijaTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            URL url = null;
            try {
                url = new URL("https://avadmin.azurewebsites.net/korisnikRegistracija");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            String s=null;
            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("ime", ime.getText().toString())
                        .appendQueryParameter("prezime", prezime.getText().toString())
                        .appendQueryParameter("email", email.getText().toString())
                        .appendQueryParameter("username", username.getText().toString())
                        .appendQueryParameter("lozinka", lozinka.getText().toString());

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(builder.build().getEncodedQuery());
                writer.flush();
                writer.close();
                os.close();

                connection.connect();

                InputStream is = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                s = br.readLine();
                br.close();


            } catch (IOException e) {
                e.printStackTrace();
            }
            return s;
        }



        @Override
        protected void onPostExecute(String s) {
            pb.setVisibility(View.GONE);
            if(s.equals("OK")) {
                Toast.makeText(RegistracijaActivity.this, R.string.uspjesna_reg, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(RegistracijaActivity.this, LoginActivity.class);
                startActivity(intent);
            }
            else Toast.makeText(RegistracijaActivity.this,s, Toast.LENGTH_SHORT).show();
        }
    }
}
